#! /usr/bin/python3
import socket
import sys
import time
try:
    R = sys.argv[1]
except:
    R = "default"
print("Stringa: "+ R)
try:
    NM = sys.argv[2]
except:
    NM = "Marco"
print("User:" +NM)
S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
PORT = 65432
S.connect(('', PORT))
S.send(R.encode())
DATA = S.recv(1024).decode()
print("stringa 'palindromizzata': " + DATA)
S.send(NM.encode())
ACCESS = S.recv(1024).decode()
if ACCESS == "error":
    print("non esiste alcun " + NM + " nel database")
else:
    print(NM + " ha acceduto " + ACCESS + " volte")
S.shutdown(0)
S.close()
time.sleep(2)
print()
