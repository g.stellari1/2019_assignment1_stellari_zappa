import os
import random
import string
import sys
import threading
import time


def RandomString(StringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(StringLength))
def ServerT():
    #print("siamo in Server")
    #print(os.listdir())
    #print(os.getcwd())
    os.system('python server.py')
    #subprocess.Popen("server.py",shell=False)
def ClientT(Usr):
    str = RandomString()
    #print("siamo in Client")
    #print(os.listdir())
    #print(os.getcwd())
    os.system('python client.py ' + str + " " + Usr)
    #subprocess.Popen("client.py Giorgio "+Str,shell=False)

try:
    USR = sys.argv[1]
except:
    USR = "Marco"
CONT: int = 0

for i in range(0, 10):
    print(CONT + 1)
    thread1 = threading.Thread(target=ServerT, daemon=True)
    thread1.start()
    print("server online")
    time.sleep(1)
    ClientT(USR)
    while thread1.is_alive():
        f = 1
    CONT = CONT + 1
    time.sleep(1)

print("sono state eseguite " + CONT.__str__() + " su 10 iterazioni")
