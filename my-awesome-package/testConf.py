import pytest
from bocadillo import configure, create_client, LiveServer

from app import app

configure(app)

@pytest.fixture
def app():
    return app

@pytest.fixture
def client(app):
    return create_client(app)

@pytest.fixture
def server(app):
    with LiveServer(app) as server:
        yield server