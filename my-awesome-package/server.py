import socket as sk
import sys
from threading import Thread
import pandas

HOST = ''  # Standard loopback interface address (localhost)
PORT = 65432  # Port to listen on (non-privileged ports are > 1023)


SERVERSOCKET = sk.socket(sk.AF_INET, sk.SOCK_STREAM)
#print(host)
#print(port)
SERVERSOCKET.bind((HOST, PORT))


class Client(Thread):
    def __init__(self, sock, address):
        Thread.__init__(self)
        self.sock = sock
        self.addr = address
        self.start()

    def run(self):
        data = self.sock.recv(1024).decode()
        #print('Client sent:', data)
        agg = data[:-1]
        tot = data + agg[::-1]
        self.sock.send(tot.encode())
        nome = self.sock.recv(1024).decode()
        #print('Target:', nm)
        csv = pandas.read_csv("DS_Assignment1.csv", delimiter=",")
        # print(CSV)
        csv = pandas.DataFrame(csv)
        # print("dataframe:")
        # print(CSV)
        res = csv[nome == csv["nome"]]
        #print(res.empty)
        if res.empty:
            print("no such user on our database")
            self.sock.send("error".encode())
            SERVERSOCKET.close()
            sys.exit()

        else:
        #print("risultati:")
        #print( res)
            res = res["accessi"].values[0] + 1
        #print("n accessi: " + res.__str__())

            self.sock.send((res.__str__()).encode())
        #print(CSV[CSV["nome"] == nm])
            csv.loc[csv["nome"] == nome, "accessi"] = res
        # print(CSV)
        #print(res)

            final = csv[["nome", "accessi"]]
        #print(final)
            final.to_csv("DS_Assignment1.csv", sep=",")
            #SERVERSOCKET.shutdown(0)
            SERVERSOCKET.close()


SERVERSOCKET.listen(5)
print('server started and listening')
print()
CLIENTSOCKET, ADDRESS = SERVERSOCKET.accept()
Client(CLIENTSOCKET, ADDRESS)
