import os

from setuptools import find_packages, setup
from package import Package
if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG']
else:
    version = os.environ['CI_JOB_ID']
setup(
    name="Assignment1",
    version=version,
    author="Giorgio Stellari",
    author_email="g.stellari1@campus.unimib.it",
    packages=find_packages(),
    include_package_data=True,
    cmdclass={
        "package": Package
    }
)