# 2019_assignment1_stellari_zappa

Il progetto in una applicazione Client/Server (con lato server collegato a csv locale) in
cui gli argomenti del client (stringa + Utente) vengono inviati al server che 
restituisce la stringa "pallindromizzata" ed aumenta un contatore nel csv dove si 
visualizzano il numero di accessi dell’utente specificato nel client.

Legenda:
    1)Build: Installazione requirement necessari al funzionamento della pipeline; 
    2)Verify: Utilizzo di pylint per la sintassi dei file .py;
    3)Integration-Test: Esecuzione di test.py con e senza user(riferisi al codice);
    4)Package: Creazione del package tramite tarfile(riferisi alla documentazione python);
    5)Deploy: Caricamento progetto su ambiente Heroku.

Esempio:
codice cmd:
python Server.py
python Client.py ciao Luca
Se il client digita la parola “ciao” il server restituirà la stringa “ciaoaic” e il numero di volte in cui l'utente ha usufruito del servizio.
Link Gitlab: https://gitlab.com/g.stellari1/2019_assignment1_stellari_zappa

A causa di recenti problemi nell'esecuzione della Pipeline dovuta ai server gitlab , controllare l'ultima pipeline corretta in caso di fail. 

A causa di alcuni disguidi con la cache di gitlab, prima di avviare la pipeline è necessario svuotare la cache manualmente. Per svuotare la cache
procedere sul menù a tendina a sinistra fino alla voce CI/CD, selezionare Pipelines per aprire l'interaccia e premere il pulsante 
"Clear Runner Caches" in alto a destra. 