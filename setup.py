import os

from package import Package
from setuptools import find_packages, setup

if os.environ.get('CI_COMMIT_TAG'):
    version = os.environ['CI_COMMIT_TAG']
else:
    version = os.environ['CI_JOB_ID']

setup(
    name='my-awesome-package',
    version=version,
    description='My awesome package',
    author='Stellari Giorgio, Luca Zappa',
    author_email="g.stellari1@campus.unimib.it",
    packages=find_packages(),
    include_package_data=True,
    url='https://gitlab.com/g.stellari1/2019_assignment1_stellari_zappa',
    cmdclass={
        "package": Package
    }
)